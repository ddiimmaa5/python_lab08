# Запускати окремо
# Task1
import matplotlib.pyplot as plt
import numpy as np

a = int(input("Введіть значення а: "))
b = int(input("Введіть значення b: "))
c = int(input("Введіть значення c: "))
d = int(input("Введіть значення d: "))
f = int(input("Введіть значення f: "))
x = np.linspace(c, d, f)
y = np.sin((a * x ** 2) + b)
plt.plot(x, y)
plt.grid()
plt.show()
# Task2
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import UnivariateSpline

a = int(input("Введіть значення а: "))
b = int(input("Введіть значення b: "))
c = int(input("Введіть значення c: "))
d = int(input("Введіть значення d: "))
f = int(input("Введіть значення f: "))
m = int(input("Введіть значення m: "))
x = np.linspace(c, d, f)
y = np.sin((a * x ** 2) + b)
plt.plot(x, y)
plt.grid()
plt.show()
xs = np.linspace(c, d, f)
spl = UnivariateSpline(x, y)
plt.plot(xs, spl(xs), 'r', lw=m)
plt.grid()
plt.show()
# Task3
import numpy as np

a = float(input("Введіть значення а: "))
b = float(input("Введіть значення b: "))
res_a = np.array([[a, 3 * b, 5], [2 * a, 5 * b, 1], [2 * a, 3 * b, 8]])
res_b = np.array([18, 10, 5])
try:
    x = np.linalg.solve(res_a, res_b)
except np.linalg.LinAlgError:
    x = np.linalg.lstsq(res_a, res_b)[0]
print(f'Розв\'язок першого рівняння: {round(x[0], 3)}')
print(f'Розв\'язок другого рівняння: {round(x[1], 3)}')
print(f'Розв\'язок третього рівняння: {round(x[2], 3)}')
# Task4
import matplotlib.pyplot as plt
from scipy import ndimage
import cv2
import os

corn = int(input("Задайте кут повороту: "))
zalik = cv2.imread('zalik.png')[:, :, ::-1]
zalik2 = ndimage.rotate(zalik, corn)
task4 = plt.figure(num=None, figsize=(15, 15), dpi=80, facecolor='w', edgecolor='k')
plt.imshow(zalik2)
plt.show()
if not os.path.isdir(r'task4'):
    os.mkdir(r'task4')
task4.savefig(r'task4\res_zalik.png')
# Task5
import matplotlib.pyplot as plt
from scipy import ndimage
import cv2

blur = int(input("Задайте значеня розмиття: "))
noise = cv2.imread('zalik.png')
blur_zalik = ndimage.gaussian_filter(noise, blur)
task5 = plt.figure()
plt.imshow(cv2.cvtColor(blur_zalik, cv2.COLOR_HSV2RGB))
plt.title(f'Gaussian Filter, $\sigma = {blur} pixels$')
plt.show()
cv2.imwrite('cat_gauss.png', blur_zalik)